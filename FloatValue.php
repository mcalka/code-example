<?php

use Enum\CompareTypeEnum;

class FloatValue
{
    private const DEFAULT_DECIMAL = 2;

    /**
     * @var string
     */
    private $toStringDecimalPoint = ",";
    /**
     * @var string
     */
    private $toStringThousandsSep = " ";
    /**
     * @var int
     */
    private $toStringDecimal = self::DEFAULT_DECIMAL;

    /**
     * @var float
     */
    private $value;

    /**
     * @var string
     */
    private $toStringSuffix = "";

    /**
     * FloatValue constructor.
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->value = (float)$value;
    }


    /**
     * @param string $value
     * @return static
     */
    public static function createFromString(string $value): self
    {
        $value = str_replace(" ", "", $value);
        $value = str_replace(",", ".", $value);
        return new self((float)$value);
    }

    /**
     * @param int $value
     * @param int $decimal
     * @return static
     */
    public static function createFromInt(int $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $value /= pow(10, $decimal);
        return new self((float)$value);
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString($this->toStringDecimal, $this->toStringDecimalPoint, $this->toStringThousandsSep) . $this->toStringSuffix;
    }

    /**
     * @param int    $decimal
     * @param string $decimalPoint
     * @param string $thousandsSep
     * @return string
     */
    public function toString(int $decimal = self::DEFAULT_DECIMAL, string $decimalPoint = ".", string $thousandsSep = ""): string
    {
        return number_format($this->value, $decimal, $decimalPoint, $thousandsSep);
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function subtract(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff = $this->toInt($decimal) - $value->toInt($decimal);
        $diff /= pow(10, $decimal);
        return new self($diff);
    }

    /**
     * @param int $decimal
     * @return int
     */
    public function toInt(int $decimal = self::DEFAULT_DECIMAL): int
    {
        if ($decimal < 0) {
            $decimal = 0;
        }
        return (int)round($this->value * pow(10, $decimal));
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return string
     */
    public function add(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): string
    {
        $diff = $this->toInt($decimal) + $value->toInt($decimal);
        $diff /= pow(10, $decimal);
        return new self($diff);
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function incrementBy(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff        = $this->toInt($decimal) + $value->toInt($decimal);
        $diff        /= pow(10, $decimal);
        $this->value = $diff;
        return $this;
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function decrementBy(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff        = $this->toInt($decimal) - $value->toInt($decimal);
        $diff        /= pow(10, $decimal);
        $this->value = $diff;
        return $this;
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function multiplyBy(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff        = $this->toInt($decimal) * $value->toInt($decimal);
        $diff        /= pow(10, $decimal);
        $diff        /= pow(10, $decimal);
        $this->value = $diff;
        return $this;
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function divideBy(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff        = $this->toInt($decimal) / $value->toInt($decimal);
        $this->value = $diff;
        return $this;
    }

    /**
     * @param \FloatValue $value
     * @param int         $decimal
     * @return $this
     */
    public function divide(FloatValue $value, int $decimal = self::DEFAULT_DECIMAL): self
    {
        $diff = $this->toInt($decimal) / $value->toInt($decimal);
        return new FloatValue($diff);
    }


    /**
     * @param FloatValue $compare
     * @param int        $operation
     * @return bool
     */
    public function compare(FloatValue $compare, int $operation = CompareTypeEnum::EQ): bool
    {
        switch ($operation) {
            case CompareTypeEnum::GT:
                return $this->toInt() > $compare->toInt();
            case CompareTypeEnum::GTE:
                return $this->toInt() >= $compare->toInt();
            case CompareTypeEnum::LT:
                return $this->toInt() < $compare->toInt();
            case CompareTypeEnum::LTE:
                return $this->toInt() <= $compare->toInt();
            case CompareTypeEnum::EQ:
            default:
                return $this->toInt() === $compare->toInt();
        }
    }

    /**
     * @param string $toStringDecimalPoint
     * @return $this
     */
    public function setToStringDecimalPoint(string $toStringDecimalPoint): self
    {
        $this->toStringDecimalPoint = $toStringDecimalPoint;
        return $this;
    }

    /**
     * @param string $toStringThousandsSep
     * @return $this
     */
    public function setToStringThousandsSep(string $toStringThousandsSep): self
    {
        $this->toStringThousandsSep = $toStringThousandsSep;
        return $this;
    }

    /**
     * @param int $toStringDecimal
     * @return $this
     */
    public function setToStringDecimal(int $toStringDecimal): self
    {
        $this->toStringDecimal = $toStringDecimal;
        return $this;
    }

    /**
     * @param string $suffix
     * @return $this
     */
    public function setToStringSuffix(string $suffix): self
    {
        $this->toStringSuffix = $suffix;
        return $this;
    }

    /**
     * @return $this
     */
    public function abs(): self
    {
        $this->value = abs($this->value);
        return $this;
    }

}
