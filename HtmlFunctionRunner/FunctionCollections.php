<?php

namespace HtmlFunctionRunner;

use HtmlFunctionRunner\Exceptions\FunctionNameDuplicateException;
use HtmlFunctionRunner\Exceptions\UnknownFunctionNameException;
use HtmlFunctionRunner\HtmlFunction\FunctionInterface;

class FunctionCollections
{
    /**
     * @var array|FunctionInterface[]
     */
    private $data;

    /**
     * FunctionCollections constructor.
     */
    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param string $functionClass
     * @throws \HtmlFunctionRunner\Exceptions\FunctionNameDuplicateException
     * @throws \ReflectionException
     */
    public function add(string $functionClass): void
    {
        $rc = new \ReflectionClass($functionClass);
        if (!$rc->isSubclassOf(FunctionInterface::class)) {
            throw new FunctionNameDuplicateException();
        }
        $name = $rc->newInstanceWithoutConstructor()->getName();
        if ($this->isset($name)) {
            throw new FunctionNameDuplicateException();
        }
        $this->data[$name] = $functionClass;
    }

    /**
     * @param string $name
     * @return bool
     */
    private function isset(string $name): bool
    {
        return isset($this->data[$name]);
    }

    /**
     * @param \Closure $function function(FunctionInterface $value, string $name)
     * @throws \HtmlFunctionRunner\Exceptions\UnknownFunctionNameException
     */
    public function each(\Closure $function): void
    {
        foreach ($this->data as $name => $value) {
            $object = $this->get($name);
            $function($object, $name);
        }
    }

    /**
     * @param string $name
     * @return \HtmlFunctionRunner\HtmlFunction\FunctionInterface
     * @throws \HtmlFunctionRunner\Exceptions\UnknownFunctionNameException
     */
    public function get(string $name): FunctionInterface
    {
        if (!$this->isset($name)) {
            throw new UnknownFunctionNameException();
        }
        return new $this->data[$name]();
    }
}