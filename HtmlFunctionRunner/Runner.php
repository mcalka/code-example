<?php

namespace HtmlFunctionRunner;

use HtmlFunctionRunner\HtmlFunction\FunctionInterface;

class Runner
{
    private $functions;
    /** @var \User */
    private $user;
    /**
     * @var \RunnerConfig
     */
    private $config;

    /**
     * Runner constructor.
     * @param \User         $user
     * @param \RunnerConfig $config
     */
    public function __construct(\User $user, \RunnerConfig $config)
    {
        $this->functions = new FunctionCollections();
        $this->user      = $user;
        $this->config    = $config;
    }

    /**
     * @param string $function
     * @throws \HtmlFunctionRunner\Exceptions\FunctionNameDuplicateException
     * @throws \ReflectionException
     */
    public function registerFunction(string $function): void
    {
        $this->functions->add($function);
    }

    /**
     * @param string $name
     * @param array  $arguments
     * @return mixed
     * @throws \HtmlFunctionRunner\Exceptions\UnknownFunctionNameException
     */
    public function __call(string $name, array $arguments)
    {
        $function = $this->functions->get($name);
        $function->setUser($this->user);
        $function->parseArguments($arguments);
        return $function->run();
    }

    /**
     * @return string
     * @throws \HtmlFunctionRunner\Exceptions\UnknownFunctionNameException
     */
    public function getHtmlInfo(): string
    {
        $info = [];
        $this->functions->each(function (FunctionInterface $function) use (&$info) {
            $info[] = $function->getDescription();
        });
        $glue = $this->config->isInfoAsHtml() ? \CHtml::newLine() : PHP_EOL;
        return join($glue, $info);
    }
}