<?php

namespace HtmlFunctionRunner\Factory;

use HtmlFunctionRunner\Runner;

abstract class BaseHtmlFunctionRunnerFactory implements HtmlFunctionRunnerFactoryInterface
{

    /**
     * @return \HtmlFunctionRunner\Runner
     */
    abstract public function getRunner(): Runner;
}