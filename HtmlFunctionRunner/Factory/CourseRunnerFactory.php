<?php

namespace HtmlFunctionRunner\Factory;

use HtmlFunctionRunner\HtmlFunction\CreateCompanyFunction;
use HtmlFunctionRunner\HtmlFunction\Generator\CompanyNameGeneratorFunction;
use HtmlFunctionRunner\Runner;
use User;

class CourseRunnerFactory extends BaseHtmlFunctionRunnerFactory
{
    /**
     * @var string[]
     */
    public static $functions = [
        //Generators
        CompanyNameGeneratorFunction::class,

        //Function
        CreateCompanyFunction::class,
    ];
    /**
     * @var User
     */
    private $user;

    /**
     * CourseFunctionRunnerFactory constructor.
     * @param \User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \HtmlFunctionRunner\Runner
     * @throws \HtmlFunctionRunner\Exceptions\FunctionNameDuplicateException
     * @throws \ReflectionException
     */
    public function getRunner(): Runner
    {
        $functionRunner = new Runner($this->user, \RunnerConfig::get());

        foreach (self::$functions as $functionClass) {
            $functionRunner->registerFunction($functionClass);
        }
        return $functionRunner;
    }
}