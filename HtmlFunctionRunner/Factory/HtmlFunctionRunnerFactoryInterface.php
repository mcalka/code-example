<?php

namespace HtmlFunctionRunner\Factory;

use HtmlFunctionRunner\Runner;

interface HtmlFunctionRunnerFactoryInterface
{
    /**
     * @return \HtmlFunctionRunner\Runner
     */
    public function getRunner(): Runner;
}