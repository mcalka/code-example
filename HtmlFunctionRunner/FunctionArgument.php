<?php


namespace HtmlFunctionRunner;


class FunctionArgument
{
    /**
     * @var string
     */
    protected $name        = "";
    /**
     * @var string
     */
    protected $description = "";

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FunctionArgument
     */
    public function setName(string $name): FunctionArgument
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return FunctionArgument
     */
    public function setDescription(string $description): FunctionArgument
    {
        $this->description = $description;
        return $this;
    }

}