<?php

namespace HtmlFunctionRunner\HtmlFunction;

use Company;
use HtmlFunctionRunner\FunctionArgument;
use T;

class CreateCompanyFunction extends BaseFunction
{
    /**
     * @var array
     */
    private $companyData = [];

    public function __construct()
    {
        $this->registerArgument((new FunctionArgument())->setName('data')->setDescription(T::tr('Dane firmy')));
    }

    public function parseArguments(array $arguments): void
    {
        [$this->companyData] = $arguments;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $company = new Company();
        $company->setAttributes($this->companyData);
        $company->save();
        return $company;
    }

    public function getName(): string
    {
        return 'createCompany';
    }

}