<?php


namespace HtmlFunctionRunner\HtmlFunction;


use CHtml;
use HtmlFunctionRunner\Exceptions\NoUserDefinedException;
use HtmlFunctionRunner\FunctionArgument;
use ObjectHelper;
use T;
use User;

abstract class BaseFunction implements FunctionInterface
{
    /** @var User */
    private $user;
    /** @var array|FunctionArgument[] */
    private $arguments = [];

    /**
     * @param array $arguments
     */
    public function parseArguments(array $arguments): void
    {

    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        $functionName = $this->getName();
        if ($this->arguments) {
            $functionName .= sprintf('(%s)', join(", ", ObjectHelper::getAttrValue($this->arguments, 'name')));
        }
        $desc = CHtml::tag('code', [], $functionName);
        $desc .= CHtml::newLine();
        if (count($this->arguments) > 0) {
            $desc .= CHtml::openTag('table', ['class' => 'table']);
            $desc .= CHtml::tag('tr', [], CHtml::tag('th', [], T::tr('Nazwa argumentu')) . CHtml::tag('th', [], T::tr('Opis argumentu')));
            foreach ($this->arguments as $argument) {
                $desc .= CHtml::tag('tr', [], CHtml::tag('td', [], $argument->getName()) . CHtml::tag('td', [], $argument->getDescription()));
            }
            $desc .= CHtml::closeTag('table');
        } else {
            $desc .= CHtml::newLine();
        }
        return CHtml::tag('div', [], $desc);
    }

    /**
     * @return \User
     * @throws \HtmlFunctionRunner\Exceptions\NoUserDefinedException
     */
    protected function getUser(): User
    {
        if (null === $this->user) {
            throw new NoUserDefinedException();
        }
        return $this->user;
    }

    /**
     * @param \User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    protected function isUserSet(): bool
    {
        return !empty($this->user);
    }

    /**
     * @param \HtmlFunctionRunner\FunctionArgument $argument
     */
    protected function registerArgument(FunctionArgument $argument): void
    {
        $this->arguments[] = $argument;
    }
}