<?php

namespace HtmlFunctionRunner\HtmlFunction\Generator;

use HtmlFunctionRunner\FunctionArgument;
use HtmlFunctionRunner\HtmlFunction\BaseFunction;
use T;

class CompanyNameGeneratorFunction extends BaseFunction
{
    /**
     * @var int
     */
    private $length;

    public function __construct()
    {
        $this->registerArgument((new FunctionArgument())->setName('length')->setDescription(T::tr('Długość wygenerowanej nazwy')));
    }

    public function parseArguments(array $arguments): void
    {
        [$this->length] = $arguments;
        $this->length = (int)($this->length ?? 4);
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $available = range("A", "Z");
        $indexes   = array_keys($available);
        $name      = "";
        for ($i = 0; $i < $this->length; $i++) {
            $name .= $available[array_rand($indexes)];
        }
        return $name;
    }

    public function getName(): string
    {
        return 'companyNameGenerator';
    }
}