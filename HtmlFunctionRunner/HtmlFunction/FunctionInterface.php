<?php


namespace HtmlFunctionRunner\HtmlFunction;


interface FunctionInterface
{

    /**
     * @param \User $user
     */
    public function setUser(\User $user): void;

    /**
     * @return mixed
     */
    public function run();

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param array $arguments
     */
    public function parseArguments(array $arguments): void;

    /**
     * @return string
     */
    public function getDescription(): string;
}