<?php


class RunnerConfig
{
    use SingletonTrait;

    /**
     * @var bool
     */
    private $infoAsHtml = false;

    /**
     * @return bool
     */
    public function isInfoAsHtml(): bool
    {
        return $this->infoAsHtml;
    }

    protected function initData(): void
    {
        $this->infoAsHtml = true;
    }

}