<?php


trait SingletonTrait
{
    private static $instance;

    private function __construct()
    {
        $this->initData();
    }

    /**
     * @return void
     */
    abstract protected function initData(): void;

    /**
     * @return static
     */
    public static function get()
    {
        if (self::$instance) {
            return self::$instance;
        }
        self::$instance = new self;
        return self::$instance;
    }

    private function __clone()
    {
        throw new Exception('This class cannot be cloned');
    }


}