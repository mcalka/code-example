<?php

namespace Enum;

interface EnumInterface
{

    /**
     * @return array
     */
    public static function getAll(): array;

    /**
     * @param int $enum
     * @return string
     */
    public static function getLabel(int $enum): string;
}