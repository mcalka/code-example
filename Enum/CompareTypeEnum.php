<?php

namespace Enum;

use T;

class CompareTypeEnum extends BaseEnum implements EnumInterface
{
    public const EQ  = 1;
    public const GT  = 2;
    public const GTE = 3;
    public const LT  = 4;
    public const LTE = 5;

    /**
     * @return array|string[]
     */
    public static function getAll(): array
    {
        return [
            self::EQ  => T::tr('równy'),
            self::GT  => T::tr('większy'),
            self::GTE => T::tr('równy i większy'),
            self::LT  => T::tr('mniejsz'),
            self::LTE => T::tr('równy i mniejszy'),
        ];
    }
}