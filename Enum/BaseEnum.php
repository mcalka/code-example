<?php

namespace Enum;

abstract class BaseEnum
{
    /**
     * @param int $enum
     * @return string
     */
    public static function getLabel(int $enum): string
    {
        $options = static::getAll();
        return isset($options[$enum]) ? $options[$enum] : $enum;
    }

    /**
     * @return array
     */
    abstract public static function getAll(): array;

    /**
     * @return array
     */
    public static function getAllKeys(): array
    {
        return array_keys(static::getAll());
    }

}