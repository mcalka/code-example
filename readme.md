### Przykład kodu - Marcin Całka

Powyższe klasy wyciągnałem z działającego projektu ale traktowałbym je raczej jako pseudokod

 - Klasy z pliku ``Mocked\Mocked.php`` dodałem tylko aby uspkoić IDE
 - Klasa ``FloatValue`` jest implementacją wzorca obiektowanego ValueObject - pozwala on odseparowanie i ujednolicenie operacji związanych z jednym typem w tym przypadku na float. Niweluje ona problemu z operacjami na liczbach zmiennoprzecinkowych 
 - Klasy z ``Enum\*`` służą jako uproszczona implementacja obiektów Enum znanych z innych języków obiektowych, tutaj głównie pozwala na wyciągniecie i wygodne wielokrotne używanie tych samych stałych.
 - Klasy z namespace ``HtmlFunctionRunner`` składają się na generator prostych operacji które może wykonać użytkownik wypełniając formularz. Do klasy formularza poprzez odpowiednią fabrybkę jest dostraczaną instancja ``Runner``, za pośrednictwem, której użytkownik może wywołać dostarczone funkcje.    
